#!/usr/bin/env python
from time import sleep
import json
import requests
import os
import fileinput
import subprocess
import logging
import signal
def main():
    p = subprocess.call(["ipmiseld"])
    while True:
        r = requests.put('some_api_request')
        getted_data = json.loads(r.content)
        nodes = getted_data.get('data')
        nodes_ipmi = []
        if nodes:
            nodes_ipmi = ','.join(['{}-ipmi'.format(val) for val in nodes])
            #nodes_ipmi = nodes_ipmi.replace("n01p[1","n01p[001")
            nodes_ipmi_format = 'hostname '+nodes_ipmi
            template = """{0}
username ADMIN
password ADMIN
verbose-count 1
system-event-format "SEL System Event(%h): %d, %t, %I, %s, %T, %k, %E"
poll-interval 180
session-timeout 10000
log-facility LOG_LOCAL5
""".format(nodes_ipmi_format)
        else:
            logging.warning('Error while try to get nodes by API\n{0}'.format(nodes))
        if os.path.isdir(freeipmi_path):
            os.chdir('/etc/freeipmi/')
            with open('ipmiseld.conf', 'r') as getline:
                firstLine = getline.readline()
            nodes_ipmi_format = nodes_ipmi_format.strip().split(',')
            firstLine = firstLine.strip().split(',')
            logging.debug('nodes_ipmi_format: {0}'.format(nodes_ipmi_format))
            logging.debug('firstLine: {0}'.format(firstLine))
            if firstLine == nodes_ipmi_format:
                logging.info('No new nodes added')
            else:
                logging.info('Creating new ipmiseld.conf')
                with open('ipmiseld.conf', 'w') as f:
                    f.write(template)
                try:
                    pid = map(int, subprocess.check_output(['pgrep','-xf','ipmiseld']).split())
                    for i in pid:
                        logging.info('try for i in pid')
                        os.kill(i, signal.SIGTERM)
                        logging.debug('Terminating ipmiseld by pid {0}'.format(i))
                    p = subprocess.call(["ipmiseld"])
                    logging.debug('New ipmiseld process spawned.')
                except subprocess.CalledProcessError:
                    logging.warning('PID of ipmiseld not found... Starting ipmiseld')
                    p = subprocess.call(["ipmiseld"])
        else:
            logging.warning('Directory {0} not found'.format(freeipmi_path))
        sleep(3600)
if __name__ == '__main__':
    freeipmi_path = '/etc/freeipmi'
    logging.basicConfig(filename='/var/log/scripterror.log', format='%(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=logging.DEBUG)
    signal.signal(signal.SIGCHLD, signal.SIG_IGN)
    main()