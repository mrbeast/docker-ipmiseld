FROM centos:7.3.1611
LABEL maintainer "Sergey Lazarev <serj.v.lazarev@gmail.com>"
RUN yum -y update && yum install -y epel-release
RUN yum -y update && yum install -y libgcrypt-devel wget gcc make python-pip \
  && cd /tmp && wget http://ftp.gnu.org/gnu/freeipmi/freeipmi-1.5.5.tar.gz \
  && tar xvf freeipmi-1.5.5.tar.gz && cd freeipmi-1.5.5/ \
  && ./configure --prefix=/usr --exec-prefix=/usr --sysconfdir=/etc --localstatedir=/var --mandir=/usr/share/man && make && make install
RUN pip install requests
ENV LD_LIBRARY_PATH="/usr/lib"
COPY ./ipmi_agent.py ipmi_agent.py
RUN chmod +x ipmi_agent.py
ENTRYPOINT ["./ipmi_agent.py"]